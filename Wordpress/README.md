# Wordpress

This is the documentation on how to run the Wordpress container. 

## Prerequisites

All that is needed is a machine and to have this repo cloned to it.

## Running the file

The Docker Compose file is set up to pull the following images:
- Wordpress
- MySQL
- PHPmyadmin

In additon to this, a user with password for both MySQL and a database is created. These are easily identified in `docker-compose.yaml` and can be altered should you with to change them. In order to execute this file, the command `docker-compose up -d` should be run while in the same directory as the Docker Compose file. Then, if the user goes to `localhost:8000` from their browser, the Wordpress installation page should be displayed, ready to create a blog user. I created a sample user in order to show how the blog should appear. This can also be done by going back to `localhost:8000` once the user has been created. This is how it should look:

![](wordpress.png) 

To log into the database, the container needs to be logged into first. To do this, use `docker ps` to list the containers that are running. Identify the MySQL container and use `docker exec -it <container ID> /bin/bash` to ssh into the container- the Container Name may also be used in place of the ID. Once this has been done, sign into the MySQL user using `mysql -u fernando -p`, enter 'powerwalk' as the password (given in the docker-compose file) and then access the database created using `USE wordpress;`. From here, Wordpress data can be accessed and altered. For example, the command `SHOW TABLES;` can be run in order to see what data is stored and Wordpress users can be identified by displaying the table of users using the command `DESCRIBE wp_users;`. 

## Resiliency

The resiliency of this container is derived from the volumes section of the docker-compose file. Volumes provide data persistance since, when a container is stopped or removed, the data within will remain once the container is started again.


## Testing 

In order to test the database that Wordpress uses, I made changes to the example post on Wordpress in my browser. Then, from my terminal which was logged into the database, I used the command `SELECT * FROM wp_posts;`. I could see the changes that I had made to the post which verified that everything is connected and working as it should. In addition to this, a colleauge cloned the repo and ran the necessary commands to ensure that the client's developers can easily make use of this repo.



